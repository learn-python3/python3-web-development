from flask import Flask, render_template
app = Flask(__name__)  # __name__ = 'main'


@app.route('/')
def handle_route_default():
    return render_template('index.html')


@app.route('/<string:fragment>')
def handle_route_fragment(fragment):
    print('fragment: ', fragment)
    return render_template(f'{fragment}.html')


@app.route('/submit_form', methods=['POST', 'GET'])
def submit_form():
    return 'form submitted!'
